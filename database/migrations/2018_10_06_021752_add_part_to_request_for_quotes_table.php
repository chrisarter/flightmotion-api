<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartToRequestForQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_for_quotes', function (Blueprint $table) {
            $table->integer('part_id')->nullable();
            $table->integer('contact_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->json('conditions')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_for_quotes', function (Blueprint $table) {
            //
        });
    }
}
