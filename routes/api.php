<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/rfqs', 'RequestForQuoteController@store');

Route::post('/webhooks/quickbooks', 'QuickBooksController@intake');
Route::post('/webhooks/quickbooks/auth', function(Request $request) {
    Log::info($request);
});
Route::post('/webhooks/stripe', 'StripeController@intake');

Route::get('/querytest', function(Request $request){
    return collect(['foo'=>'bar'])->toJson();

});


Route::resource('inventory-items', 'InventoryItemController');
