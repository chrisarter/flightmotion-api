<?php
use App\Part;
use App\Company;
use App\Contact;
use App\InventoryItem;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\RequestForQuote;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/inventory/{id}', 'InventoryItemController@show')->name('inventoryitems.show');


// Route::get('/test/cust', function(){
// $customers = array_map('str_getcsv', file( public_path() . '/cust.csv') );

// foreach($customers as $customer){

//     $company = Company::where('name', $customer[0])->get();

//     if($company->isEmpty()){

//         $company = new Company();
//         $company->name = $customer[0];
//         $company->address_line_1 = $customer[1];
//         $company->address_line_2 = $customer[2];
//         $company->address_line_3 = $customer[3];
//         $company->city = $customer[4];
//         $company->state_province = $customer[5];
//         $company->county = $customer[6];
//         $company->postal_code = $customer[7];
//         $company->country_code = $customer[8];
//         $company->phone_number = $customer[9];
//         $company->fax_number = $customer[11];
//         $company->email = $customer[13];
//         $company->save();

//     }

//     if(
//         Contact::where('first_name', $customer[15])
//         ->where('last_name',$customer[16] )->get()->isEmpty()
//         ){
//             $newContact = new Contact();
//             $newContact->first_name = $customer[16];
//             $newContact->last_name = $customer[17];
//             $newContact->title = $customer[18];
//             $newContact->phone_number = $customer[19];
//             $newContact->extension = $customer[20];
//             $newContact->cell_number = $customer[21];
//             $newContact->save();
//             $company->first()->contacts()->save($newContact);
//     } 
// }


// return 'done';

//     // $company = Company::where('name', $customer[0])->get();
//     // dump($company->isEmpty());
// });

Route::get('item/{id}', function($id){
    $item = InventoryItem::find($id);
    return $item->removedFrom;
});

Route::get('removal/{id}/', function ($id){
    $data = InventoryItem::find($id);
    $data->with('removedFrom')->with('part');
    return view('pdf.removal_tag')->with(['data' => $data]);
});