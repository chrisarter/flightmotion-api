<?php

namespace App\Nova;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Status;
use Laravel\Nova\Fields\MorphMany;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Currency;
use Flightmotion\Quotedata\Quotedata;
use Flightmotion\Quickquote\Quickquote;
use Infinety\Filemanager\FilemanagerField;
use Laravel\Nova\Panel;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Epartment\NovaDependencyContainer\NovaDependencyContainer;
use Epartment\NovaDependencyContainer\HasDependencies;

class InventoryItem extends Resource
{

    use HasDependencies;
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\InventoryItem';


    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public function title(){

        if($this->part->serial_number){
            $serial = $this->part->serial_number;
        } else {
            $serial = 'N/A';
        }
        return $this->part->part_number . ' SN: ' . $serial;
    }

    public static $with = ['removedFrom', 'part'];

    public static $searchRelations = [
        'part' => ['part_number', 'serial_number', 'description'],
    ];


    /**
     * Change subtitle
     *
     * @return string
     */
    public function subtitle(){
        
        $sub = '';
        if($this->serial_number){
            $sub .= 'Serial: ' . $this->serial_number;
        }

        if($this->removed_from_id){
            
            if($this->removedFrom->serial_number){
                $sub .= ' Removed From:' . $this->removedFrom->serial_number;
            } else {
                $sub .= ' Removed From:' . $this->removedFrom->part->part_number;
            }
        }
        return $sub;
    }
    

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [

            /**
            * Part relationship.
            */
            BelongsTo::make('Part')->searchable(),


            /**
            * Description
            */
            Text::make('Desc', function(){
                    if($this->part && $this->part->description){
                        return $this->part->description;
                    } })->withMeta(['extraAttributes' => [
                'readonly' => true
            ]]),

            /**
            * Serial Number
            */
            Text::make('Serial Number', 'serial_number'),


            /**
             * Status
             */

            Select::make('Status')->options(config('flightmotion.item_statuses')),
            /**
             * Condition
             */
            Select::make('Condition')->options(config('flightmotion.conditions')),
        

            /**
             *  Document field for general docs.
             */
            FilemanagerField::make('Documents')
                ->displayAsImage()
                ->folder('downloads')
                ->folder('/storage/inventory_items/' . $this->id . '/documents')
                ->hideFromIndex(),
        
            /**
             * Removed from
             */
            BelongsTo::make('Removed From', 'removedFrom', $this)
                        ->help('REMOVED FROM'),
                        MorphMany::make('Documents'),
            /**
             * Minipack for download. this is public.
             */
            File::make('Minipack')
                ->disk('s3-public')
                ->path('/downloads/')
                // ->path('storage/inventory_items/' . $this->id . '/minipack')
                ->storeAs(function (Request $request) {
                    return $request->minipack->getClientOriginalName();
                })->help(
                    'PDF of minipack for this unit.'
                ),
        
            /**
             * Cost
             */
            Currency::make('Cost'),
        
            /**
             * Trace Panel
             */
            new Panel('Trace', $this->traceFields()),

            /**
             * Warehouse
             */
            BelongsTo::make('Warehouse')->hideFromIndex(),

            /**
             * Shipments
             */
            BelongsToMany::make('Shipments'),

            /**
             * Quote History
             */
            HasMany::make('Quote History', 'quoteLineItems', $this),

            /**
             * Removed Parts
             */
            HasMany::make('Removed Parts', 'removedParts', $this),


            new Panel('Images', [

                Images::make('Images', 'images') // second parameter is the media collection name
                ->thumbnail('thumb') // conversion used to display the image
                ->multiple() // enable upload of multiple images - also ordering
                ->fullSize(), // full size column
            ]),
            ];

            
    }

    protected function traceFields(){
        return [

        Boolean::make('Use Assembly Trace', 'inherit_trace')
            ->hideFromIndex()
            ->help("Toggle whether to use the assembly's trace this was removed from"),
    
            NovaDependencyContainer::make([
                 /**
                 * Final trace packet, this is public.
                 */
                File::make('Trace File', 'item_trace')->help(
                    'PDF of the complete trace for this unit.'
                ),

                Select::make('Trace Type', 'item_trace_type')->options(config('flightmotion.trace_types')),

            ])->dependsOn('inherit_trace', false),


            NovaDependencyContainer::make([

                File::make('Trace File', 'trace')->withMeta(['extraAttributes' => [
                    'readonly' => true
                ]]),

                Text::make('Trace Type', 'trace_type')->withMeta(['extraAttributes' => [
                    'readonly' => true
                ]])

            ])->dependsOn('inherit_trace', true),


        ];
    }
    public static function singularLabel()
    {
        return 'Inventory Item';
    }
        /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Inventory';
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [

        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [ 
            new Filters\InventoryStatus,
            new Filters\InventoryCondition,
         ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new Actions\ShareAssets,
            new Actions\SendForRepair,
            new Actions\GenerateRemovalTag,
            new Actions\CreateDocumentsSet,
        ];
    }
}
