<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use \App\Mail\ShareAssetsMail;
use Mailgun\Mailgun;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Textarea;

class ShareAssets extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        Mail::to( ($fields->specific_emails) ? $fields->specific_emails : $fields->mailing_list )
            ->send(new ShareAssetsMail($models) );

            if(! count( Mail::failures()) > 0 ){
                Action::message('Assets Sent!'); 
            }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {

    /**
     *  Get mailgun email lists
     */
        $mailGun = new Mailgun( config('mail.mailgun.secret') );
        $results = $mailGun->get('lists/pages');
        $listOptions = [];

        foreach($results->http_response_body->items as $result){
            $listOptions[ $result->address ] = $result->name . ' (' . $result->members_count . ')';
        }
    
            return [
                Select::make('Mailing List', 'mailing_list')
                    ->options($listOptions)
                    ->displayUsingLabels(),
                Textarea::make('specific_emails')
                ->help('If you use emails in this field, it will override the email list above.'),
            ];
    }
}
