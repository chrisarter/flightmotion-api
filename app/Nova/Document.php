<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Boolean;
use Illuminate\Http\Request;

use Laravel\Nova\Http\Requests\NovaRequest;

class Document extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Document';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
        //     Text::make('Doc #', 'document_number')->withMeta(['extraAttributes' => [
        //         'readonly' => true
        //   ]]),
            Text::make('Title'),
            File::make('File', 'path'),
            Boolean::make('Visible', 'public')->help('Toggle whether this is visible to customers'),
            Select::make('Type')->options([
                'Quote' =>  'quote',
                'repair order'  =>  'Repair Order',
                'Purchase Order'    =>  'purchase order',
                'Packing Slip'  =>  'packing slip',
            ]),
            Trix::make('Content', 'content'),
            Date::make('Created', 'created_at')->withMeta(['extraAttributes' => [
                'readonly' => true
          ]])->hideFromDetail()
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new Actions\DownloadDocument
        ];
    }
}
