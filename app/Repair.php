<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Company;

class Repair extends Model
{
    public function mro(){
        return $this->belongsTo('App\Company', 'mro_id');
    }

    public function customer(){
        return $this->belongsTo('App\Company', 'customer_id');
    }

    public function inventoryItems(){
        return $this->belongsToMany('App\InventoryItem');
    }

    public function order(){
        return $this->belongsToMany('App\Order');
    }

}
