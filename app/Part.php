<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RequestForQuote;
use App\Quote;
use App\InventoryItem;

class Part extends Model
{
    protected $table = 'parts';

    public function rfqs(){
        return $this->belongsToMany('App\RequestForQuote');
    }

    public function RequestForQuotes(){
        return $this->rfqs();
    }

    public function quotes(){
        return $this->hasMany('App\Quote');
    }

    public function latestPrice(){
        $latestQuoteItem = $this->quotes->first();
        return $latestQuoteItem->amount;
    }

    public function inventoryItems(){
        return $this->hasMany('App\InventoryItem');
    }
}
