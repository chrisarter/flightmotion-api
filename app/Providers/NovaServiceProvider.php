<?php

namespace App\Providers;

use Laravel\Nova\Nova;
use Laravel\Nova\Cards\Help;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\NovaApplicationServiceProvider;
use Flightmotion\Quotedata\Quotedata;
use App\InventoryItem;
use App\Observers\InventoryItemObserver;
use App\RequestForQuote;
use App\Order;
use App\Observers\OrderObserver;
use App\Observers\RequestForQuoteObserver;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        Nova::serving(function () {
            InventoryItem::observe(InventoryItemObserver::class);
            RequestForQuote::observe(RequestForQuoteObserver::class);
            // Order::observe(OrderObserver::class);
        });
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
                ->withAuthenticationRoutes()
                ->withPasswordResetRoutes()
                ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return in_array($user->email, [
                //
            ]);
        });
    }

    /**
     * Get the cards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
            new \GijsG\SystemResources\SystemResources('ram'),
            new \GijsG\SystemResources\SystemResources('cpu'),
            new \Coreproc\NovaSystemInfoCard\SystemInfoCard(),
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
            new \Infinety\Filemanager\FilemanagerTool(),
            new \Vyuldashev\NovaPermission\NovaPermissionTool(),
            new \Spatie\TailTool\TailTool(),
            new \Cendekia\SettingTool\SettingTool,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        return [
            // ...
            
        ];
    }
}
