<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Part;

class RequestForQuote extends Model
{
    protected $table = 'request_for_quotes';
    protected $casts = ['conditions' => 'array'];
    protected $with = ['parts'];

    public function parts(){
        return $this->belongsToMany('App\Part');
    }

    

}
