<?php

namespace App\Listeners;

use App\Events\QuickBooksItemEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class QuickBooksItemMerge
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuickBooksItemEvent  $event
     * @return void
     */
    public function handle(QuickBooksItemEvent $event)
    {
        //
    }
}
