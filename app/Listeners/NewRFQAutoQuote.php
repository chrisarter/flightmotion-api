<?php

namespace App\Listeners;

use App\Events\NewRFQ;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewRFQAutoQuote
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewRFQ  $event
     * @return void
     */
    public function handle(NewRFQ $event)
    {
        //
    }
}
