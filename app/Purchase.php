<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\QualityReview;
class Purchase extends Model
{
    public function qualityReviews(){
        return $this->hasMany('App\QualityReview');
    }
}
