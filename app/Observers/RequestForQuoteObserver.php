<?php

namespace App\Observers;

use App\RequestForQuote;

class RequestForQuoteObserver
{
    /**
     * Handle the request for quote "created" event.
     *
     * @param  \App\RequestForQuote  $requestForQuote
     * @return void
     */
    public function created(RequestForQuote $requestForQuote)
    {
        //
    }

    /**
     * Handle the request for quote "updated" event.
     *
     * @param  \App\RequestForQuote  $requestForQuote
     * @return void
     */
    public function updated(RequestForQuote $requestForQuote)
    {
        //
    }

    /**
     * Handle the request for quote "deleted" event.
     *
     * @param  \App\RequestForQuote  $requestForQuote
     * @return void
     */
    public function deleted(RequestForQuote $requestForQuote)
    {
        //
    }

    /**
     * Handle the request for quote "restored" event.
     *
     * @param  \App\RequestForQuote  $requestForQuote
     * @return void
     */
    public function restored(RequestForQuote $requestForQuote)
    {
        //
    }

    /**
     * Handle the request for quote "force deleted" event.
     *
     * @param  \App\RequestForQuote  $requestForQuote
     * @return void
     */
    public function forceDeleted(RequestForQuote $requestForQuote)
    {
        //
    }
}
