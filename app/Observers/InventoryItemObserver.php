<?php

namespace App\Observers;

use App\InventoryItem;

class InventoryItemObserver
{
    /**
     * Handle the inventory item "created" event.
     *
     * @param  \App\InventoryItem  $inventoryItem
     * @return void
     */
    public function created(InventoryItem $inventoryItem)
    {
        //
    }

    /**
     * Handle the inventory item "updated" event.
     *
     * @param  \App\InventoryItem  $inventoryItem
     * @return void
     */
    public function updated(InventoryItem $inventoryItem)
    {
        //
    }

    /**
     * Handle the inventory item "deleted" event.
     *
     * @param  \App\InventoryItem  $inventoryItem
     * @return void
     */
    public function deleted(InventoryItem $inventoryItem)
    {
        //
    }

    /**
     * Handle the inventory item "restored" event.
     *
     * @param  \App\InventoryItem  $inventoryItem
     * @return void
     */
    public function restored(InventoryItem $inventoryItem)
    {
        //
    }

    /**
     * Handle the inventory item "force deleted" event.
     *
     * @param  \App\InventoryItem  $inventoryItem
     * @return void
     */
    public function forceDeleted(InventoryItem $inventoryItem)
    {
        //
    }
}
