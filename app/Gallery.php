<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    /**
     * Get all of the owning galleryable models.
     */
    public function galleryable()
    {
        return $this->morphTo();
    }
}
