<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\QuoteLineItem;
use App\Company;
use App\Contact;
use Illuminate\Notifications\Notifiable;

class Quote extends Model
{
    use Notifiable;
    public function quoteLineItems(){
        return $this->hasMany('App\QuoteLineItem');
    }

    public function items(){
        return $this->quoteLineItems();
    }

    public function contact(){
        return $this->belongsTo('App\Contact');
    }

    public function company(){
        return $this->belongsTo('App\Company');
    }
}
