<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\InventoryItem;
class Warehouse extends Model
{
    protected $table = 'warehouses';

    public function inventoryItems(){
        return $this->hasMany('App\InventoryItem');
    }
}
