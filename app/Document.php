<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\InventoryItem;

class Document extends Model
{
    public function documentable(){
        return $this->morphTo();
    }

    public function createMinipack($inventoryItem){

        $this->title = 'Minipack';
        $this->public = false;
        $this->template = 'pdf.minipack';
        $this->type = 'minipack';
        $this->documentable_type = 'App\InventoryItem';
        $this->documentable_id = $inventoryItem->id;
        $this->save();
        return $this;
    }

    public function createMaterialCert($inventoryItem){

        $this->title = 'Material Cert';
        $this->public = false;
        $this->template = 'pdf.material_cert';
        $this->type = 'minipack';
        $this->documentable_type = 'App\InventoryItem';
        $this->documentable_id = $inventoryItem->id;
        $this->save();
        return $this;

    }

    public function createTraceKit($inventoryItem){

        $this->title = 'Trace Kit';
        $this->public = false;
        $this->template = 'pdf.trace_kit';
        $this->type = 'tracekit';
        $this->documentable_type = 'App\InventoryItem';
        $this->documentable_id = $inventoryItem->id;
        $this->save();
        return $this;

    }
}
