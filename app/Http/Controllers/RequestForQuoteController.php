<?php

namespace App\Http\Controllers;

use App\RequestForQuote;
use Illuminate\Http\Request;
use App\Contact;
use App\Part;
use App\Events\NewRFQ;


class RequestForQuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return RequestForQuote::paginate();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rfq = new RequestForQuote();
        $rfq->email = $request->input('email');
 
        // $rfq->part_number = $request->input('part_number', '');
        $rfq->source = $request->input('source', 'web');
        $rfq->notes = $request->input('notes', '');
        $rfq->part_number = $request->input('part_number', '');
        $rfq->save();

        $part = Part::find($request->input('part_number'));
        if($part) {
            $part->rfqs()->save($rfq);
            
        }

        $rfq->save();
        event(new NewRFQ($rfq));
        return $rfq;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequestForQuote  $requestForQuote
     * @return \Illuminate\Http\Response
     */
    public function show(RequestForQuote $requestForQuote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequestForQuote  $requestForQuote
     * @return \Illuminate\Http\Response
     */
    public function edit(RequestForQuote $requestForQuote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequestForQuote  $requestForQuote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequestForQuote $requestForQuote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequestForQuote  $requestForQuote
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequestForQuote $requestForQuote)
    {
        //
    }
}
