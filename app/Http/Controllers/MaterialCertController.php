<?php

namespace App\Http\Controllers;

use App\MaterialCert;
use Illuminate\Http\Request;

class MaterialCertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MaterialCert  $materialCert
     * @return \Illuminate\Http\Response
     */
    public function show(MaterialCert $materialCert)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MaterialCert  $materialCert
     * @return \Illuminate\Http\Response
     */
    public function edit(MaterialCert $materialCert)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MaterialCert  $materialCert
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MaterialCert $materialCert)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MaterialCert  $materialCert
     * @return \Illuminate\Http\Response
     */
    public function destroy(MaterialCert $materialCert)
    {
        //
    }
}
