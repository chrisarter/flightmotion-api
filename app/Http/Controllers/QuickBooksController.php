<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Events\QuickBooksEstimateEvent;
use App\Events\QuickBooksItemEvent;
use App\Providers\QuickBooksServiceProvider;
class QuickBooksController extends Controller
{
        /**
         * This is the first method that gets
         * hit from the controller.
         *
         * @param Request $request
         * @return void
         */
        public function intake(Request $request){
            foreach($this->getEvents($request) as $event){
                $this->dispatch($event);
            }
        }

        /**
         * Get the events from the request object.
         *
         * @param Request $request
         * @return array
         */
        public function getEvents(Request $request){

            $events = [];
            foreach($request->eventNotifications as $eventNotification){   
                $events[] = $eventNotification['dataChangeEvent']['entities'];
            }
            return $events[0];
        }

        /**
         * Determine the method name to call to handle
         * this event
         *
         * @param array $event
         * @return string
         */
        public function getHandlerMethod($event){
            return strtolower($event['operation']) . $event['name'];
        }

        /**
         * Get the type of resource from the event;
         *
         * @param array $event
         * @return string
         */
        public function getResourceType($event){
            return $event->name;
        }

        /**
         * Call the method associated
         * with the event;
         *
         * @param array $event
         * @return void
         */
        public function dispatch($event){
            $method = $this->getHandlerMethod($event);
            return ( method_exists( $this, $method) ) ? $this->$method($event) : null;
        }

        /**
         * Dispatch the event for the resource
         * type.
         *
         * @param array $event
         * @param Event $eventClass
         * @return void
         */
        public function callEvent($event, $eventClass){
            event(new $eventClass($event));
        }


    /**
     * ESTIMATES
     */
        public function callEstimateEvent($event){
            $this->callEvent($event, QuickBooksEstimateEvent::class );
        }

        /**
         * Fire the estimate event for create.
         *
         * @return void
         */
        public function createEstimate($event){
            $this->callEstimateEvent($event);
        }

        /**
         * Fire the Create Estimate event
         *
         * @return void
         */
        public function updateEstimate($event){
            $this->callEstimateEvent($event);
        }

        /**
         * Fire the Create Estimate event
         *
         * @return void
         */
        public function deleteEstimate($event){
            $this->callEstimateEvent($event);
        }
}