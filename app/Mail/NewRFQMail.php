<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use App\RequestForQuote;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewRFQMail extends Mailable
{
    use Queueable, SerializesModels;

    public $requestForQuote;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(RequestForQuote $requestForQuote)
    {
        $this->requestForQuote = $requestForQuote;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from( ($requestForQuote->email) ? $requestForQuote->email : config('flightmotion.email_addresses.updates'))
                    ->view('mail.newrfq');
    }
}
