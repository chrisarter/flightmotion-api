<!-- 2 column layout with 10px spacing -->
<table width="600">

    <tr style="background:#f7f7f7;">
        <td width="200">
            <b>Part No.</b>
        </td>

        <td width="200">
            <b>Desc.</b>
        </td>

        <td width="100">
            <b>Cond.</b>
        </td>

        <td width="100">
            <b>Action</b>
        </td>
    </tr>

    @if(!$parts->isEmpty())
        @foreach($parts as $part)
            @include('mail.includes.asset_row', ['part' => $part])
        @endforeach
    @endif
</table>