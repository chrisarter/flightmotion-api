<tr style="border-bottom: 1px solid #333;">
        <td style="padding:20px 0px;" width="200">
            {{ $part->part->part_number }}
        </td>

        <td style="padding:20px 0px;" width="200">
            {{ $part->part->description }}
        </td>

        <td  style="padding:20px 0px;" width="100">
            {{ $part->condition }}
        </td>

        <td style="padding:20px 0px;" width="100">
            <a style="background-color:#371e5d; color: white; border-radius:8px; text-decoration:none; padding:8px; width:100%;" href="{{ route('inventoryitems.show', $part ) }}">View</a>
        </td>
</tr>