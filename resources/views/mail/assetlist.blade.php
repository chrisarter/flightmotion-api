@extends('layouts.mail')

@section('preheader')

    You have recieved a list of inventory from Flight Motion. Please, review
    this list, and select View on any item for more details.

@endsection
@section('content')
    <h2>Assets, Parts & more!</h2>
    <p>
        See our available assets & materials listed below. Click 'View' for additional information on 
        any of our assets, or call us at {{ config('flightmotion.location.phone') }}.</p>

    @include('mail.includes.asset_table', ['parts' => $parts])

@endsection

