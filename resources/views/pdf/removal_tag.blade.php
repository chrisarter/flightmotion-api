@extends('layouts.pdf')
@section('title')
Removal Tag
@endsection
@section('content')

        <div class="row">
            <div class="col-12">
                    <table class="table">
                            <thead>
                                <tr>
                                    <th>Part Number</th>
                                    <th>Description</th>
                                    <th>Serial Number</th>
                                    <th>Removed From</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $data->part->part_number }}</td>
                                    <td>{{ $data->part->description }}</td>
                                    <td>{{ $data->serial_number }}</td>
                                    <td>{{ $data->removedFrom->serial_number }}</td>
                                </tr>
                
                            </tbody>
                
                        </table>

                        <img width="100px" src="{{ url('/chrissig.png') }}">
            </div>
        </div>
        @endsection
